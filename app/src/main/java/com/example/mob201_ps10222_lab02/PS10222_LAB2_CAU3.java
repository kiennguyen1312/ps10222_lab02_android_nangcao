package com.example.mob201_ps10222_lab02;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PS10222_LAB2_CAU3 extends AppCompatActivity {

    private Button btnCheck = null;
    private EditText edtCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab2__cau3);

        mapComponent();
        setOnClickListener();
    }

    // Ánh xạ
    private void mapComponent() {
        if (btnCheck == null)
            btnCheck = findViewById(R.id.btnCheck);
        if (edtCode == null)
            edtCode = findViewById(R.id.edtCode);
    }

    // Xử lý sự kiện onClick
    private void setOnClickListener() {
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String maKhuyenMai = edtCode.getText().toString();
                if (isValid(maKhuyenMai)) {
                    Intent intent = new Intent(PS10222_LAB2_CAU3.this, Cau3Receiver.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("makhuyenmai", maKhuyenMai);
                    intent.putExtra("bai3", bundle);
                    intent.setAction("vn.com.nhapkhuyenmai");
                    sendBroadcast(intent);
                }
                else
                    Toast.makeText(PS10222_LAB2_CAU3.this,"Mã không tồn tại",Toast.LENGTH_LONG).show();
            }
        });
    }

    // Xử lý dữ liệu nhập vào
    private boolean isValid(String str) {
        String pattem = "^(MEM|VIP)[0-9]{6}$";
        if (!str.matches(pattem)) {
            return false;
        }
        return true;
    }
}
