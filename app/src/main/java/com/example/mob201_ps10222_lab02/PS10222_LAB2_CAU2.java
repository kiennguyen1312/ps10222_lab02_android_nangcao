package com.example.mob201_ps10222_lab02;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PS10222_LAB2_CAU2 extends AppCompatActivity {

    private static PS10222_LAB2_CAU2 ins;
    private EditText etName=null;
    private Button btnGreet=null;
    private TextView tvGreeting=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab2__cau2);
        ins = PS10222_LAB2_CAU2.this;
        mapComponent();
        setOnClickListener();
    }

    public static PS10222_LAB2_CAU2 getIns(){
        return ins;
    }

    //ánh xạ
    private void mapComponent(){
        if(etName==null)
            etName=findViewById(R.id.etName);
        if(btnGreet==null)
            btnGreet=findViewById(R.id.btnGreet);
        if(tvGreeting==null)
            tvGreeting=findViewById(R.id.tvGreeting);
    }

    private void setOnClickListener(){
        if(btnGreet==null)
            mapComponent();

        btnGreet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=etName.getText().toString();
                Intent intent=new Intent(PS10222_LAB2_CAU2.this,Cau2Receiver.class);
                Bundle bundle=new Bundle();
                bundle.putString("name",name);
                intent.putExtra("greetapp",bundle);
                intent.setAction("vn.com.kiennt.GREET");
                sendBroadcast(intent);
            }
        });
    }

    public void updateResult(String name){
        if(tvGreeting==null)
            mapComponent();
        tvGreeting.setText("Xin chào "+name);
    }
}
