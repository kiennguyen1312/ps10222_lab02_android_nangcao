package com.example.mob201_ps10222_lab02;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class Cau3Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("vn.com.nhapkhuyenmai")){
            Bundle bundle=intent.getBundleExtra("bai3");
            String maKhuyenMai=bundle.getString("makhuyenmai");
            if(maKhuyenMai.equals("MEM537128"))
                Toast.makeText(context,"Khuyến mãi 10%",Toast.LENGTH_LONG).show();
            else if(maKhuyenMai.equals("MEM131200"))
                Toast.makeText(context,"Khuyến mãi 20%",Toast.LENGTH_LONG).show();
            else if(maKhuyenMai.equals("VIP070400"))
                Toast.makeText(context,"Khuyến mãi 30%",Toast.LENGTH_LONG).show();
            else if(maKhuyenMai.equals("VIP020999"))
                Toast.makeText(context,"Khuyến mãi 50%",Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context,"Sai mã khuyến mãi",Toast.LENGTH_LONG).show();
        }
    }
}
