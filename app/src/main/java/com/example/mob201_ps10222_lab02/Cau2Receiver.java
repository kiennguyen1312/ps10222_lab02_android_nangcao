package com.example.mob201_ps10222_lab02;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class Cau2Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("vn.com.kiennt.GREET")) {
            Bundle bundle = intent.getBundleExtra("greetapp");
            String name = bundle.getString("name");
            name = name.toUpperCase();
            PS10222_LAB2_CAU2.getIns().updateResult(name);
        }
    }
}