package com.example.mob201_ps10222_lab02;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnCau1, btnCau2, btnCau3, btnCau4;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnhXa();

        btnCau1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB2_CAU1.class);
                startActivity(intent);
            }
        });

        btnCau2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB2_CAU2.class);
                startActivity(intent);
            }
        });

        btnCau3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB2_CAU3.class);
                startActivity(intent);
            }
        });

        btnCau4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB2_CAU4.class);
                startActivity(intent);
            }
        });
    }

    public void AnhXa(){
        btnCau1 = (Button) findViewById(R.id.btnCau1);
        btnCau2 = (Button) findViewById(R.id.btnCau2);
        btnCau3 = (Button) findViewById(R.id.btnCau3);
        btnCau4 = (Button) findViewById(R.id.btnCau4);
    }
}
