package com.example.mob201_ps10222_lab02;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.TextView;
import android.widget.Toast;

public class PS10222_LAB2_CAU4 extends AppCompatActivity {

    BroadcastReceiver receiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab2__cau4);

        // Tạo bộ lọc để lắng nghe tin nhắn gửi tới
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        // Tạo bộ lắng nghe
        receiver = new BroadcastReceiver() {
            //chú ý là dữ liệu trong tin nhắn được lưu trữ trong arg1
            @Override
            public void onReceive(Context context, Intent intent) {
                processReceive(context, intent);
            }
        };
        // Đăng ký bộ lắng nghe vào hệ thống
        registerReceiver(receiver, filter);
    }

    protected void onDestroy() {
        super.onDestroy();
        // Hủy bỏ đăng ký khi tắt ứng dụng
        unregisterReceiver(receiver);
    }

    public void processReceive(Context context, Intent intent) {
        //Thông báo khi có tin nhắn mới
        Toast.makeText(context, "Có 1 tin nhắn mới", Toast.LENGTH_LONG).show();
        TextView txtContent = (TextView) findViewById(R.id.tvMessage);
        //pdus để lấy gói tin nhắn
        String sms_extra = "pdus";
        Bundle bundle = intent.getExtras();
        //bundle trả về tập các tin nhắn gửi về cùng lúc
        Object[] objArr = (Object[]) bundle.get(sms_extra);
        String sms = "";
        //duyệt vòng lặp để đọc từng tin nhắn
        for (int i = 0; i < objArr.length; i++) {
            //lệnh chuyển đổi về tin nhắn createFromPdu
            SmsMessage smsMsg = SmsMessage.
                    createFromPdu((byte[]) objArr[i]);
            //lấy nội dung tin nhắn
            String body = smsMsg.getMessageBody();
            //lấy số điện thoại tin nhắn
            String address = smsMsg.getDisplayOriginatingAddress();
            sms += address + ":\n" + body + "\n";
        }
        //hiển thị lên giao diện
        txtContent.setText(sms);
    }
}
