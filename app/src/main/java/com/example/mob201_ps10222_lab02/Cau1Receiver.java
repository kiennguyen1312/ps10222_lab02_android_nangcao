package com.example.mob201_ps10222_lab02;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class Cau1Receiver extends BroadcastReceiver {

    public static final String TAG = "receiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG,"onReceiver");
        Bundle extra=intent.getExtras();
        if(extra!=null){
            String state=extra.getString(TelephonyManager.EXTRA_STATE);
            if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                String phoneNumber=extra.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Toast.makeText(context,phoneNumber,Toast.LENGTH_LONG).show();
            }
        }
    }
}
